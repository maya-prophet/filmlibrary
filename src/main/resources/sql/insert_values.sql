insert into role
values (1, 'Роль пользователя', 'USER'),
       (2, 'Роль работника фильмотеки', 'EMPLOYEE');

insert into directors
values (1, 'Айда Лупино', 'director'),
       (2, 'Джордж Дискант', 'director of photography'),
       (3, 'Герберт Мендельсон', 'assistant director'),
       (4, 'Тим Бёртон', 'director'),
       (5, 'Гильермо дель Торо', 'director'),
       (6, 'Хорхе Кальво', 'first assistant director'),
       (7, 'Карлос Сарагоса', 'assistant art director'),
       (8, 'Мэри Хэррон', 'director'),
       (9, 'Кассандра Кроненберг', 'third assistant director'),
       (10, 'Дженнифер Дэф', 'second assistant director'),
       (11, 'Эндрю Ши', 'first assistant director');

insert into films
values (1, 0, 8, 2000, 'Американский психопат'),
       (2, 0, 11, 2022, 'Мир Дали'),
       (3, 11, 5, 1993, 'Хронос'),
       (4, 8, 3, 2003, 'Лабиринт фавна'),
       (5, 0, 4, 1996, 'Марс атакует!'),
       (6, 0, 10, 1979, 'По следам монстра-сельдерея'),
       (7, 0, 2, 1953, 'Двоеженец'),
       (8, 0, 8, 1953, 'Попутчик');

insert into film_directors(film_id, director_id)
values (1, 8),
       (1, 9),
       (1, 10),
       (1, 11),
       (2, 8),
       (3, 5),
       (4, 5),
       (4, 6),
       (4, 7),
       (5, 4),
       (6, 4),
       (7, 1),
       (7, 2),
       (7, 3),
       (8, 1);



