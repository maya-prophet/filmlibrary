package ru.edu.springdatahomework.constant;

public interface UserRoleConstants {

    String ADMIN = "ADMIN";
    String EMPLOYEE = "EMPLOYEE";

    String USER = "USER";

}
