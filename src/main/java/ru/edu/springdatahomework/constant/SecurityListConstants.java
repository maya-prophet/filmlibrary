package ru.edu.springdatahomework.constant;

import java.util.List;

public interface SecurityListConstants {

    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
                                                "/js/**",
                                                "/css/**",
                                                "/swagger-ui/**",
                                                "/");
    List<String> FILMS_WHITE_LIST = List.of("/films");

    List<String> FILMS_PERMISSION_LIST = List.of("/films/add",
                                                 "/films/update",
                                                 "/films/delete");

    List<String> DIRECTORS_WHITE_LIST = List.of("/directors");

    List<String> DIRECTORS_PERMISSION_LIST = List.of("/directors/add",
                                                     "/directors/update",
                                                     "/directors/delete");

    List<String> USERS_WHITE_LIST = List.of("/login",
                                            "/users/registration",
                                            "/users/remember-password",
                                            "/users/change-password");
}
