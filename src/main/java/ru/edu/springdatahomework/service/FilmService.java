package ru.edu.springdatahomework.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.springdatahomework.dto.FilmDTO;
import ru.edu.springdatahomework.dto.FilmWithDirectorsDTO;
import ru.edu.springdatahomework.mapper.FilmMapper;
import ru.edu.springdatahomework.mapper.FilmWithDirectorsMapper;
import ru.edu.springdatahomework.model.Director;
import ru.edu.springdatahomework.model.Film;
import ru.edu.springdatahomework.repository.DirectorRepository;
import ru.edu.springdatahomework.repository.FilmRepository;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO>{

    protected final DirectorRepository directorRepository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository filmRepository,
                          DirectorRepository directorRepository,
                          FilmMapper mapper,
                          FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, mapper);
        this.directorRepository = directorRepository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public FilmDTO addDirector(Long filmId, Long directorId) {
        Film film = repository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Книги с таким ID не найдено"));
        Director director = directorRepository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Директора с таким ID не найдено"));
        film.getDirectors().add(director);
        return mapper.toDTO(repository.save(film));
    }

    public List<FilmWithDirectorsDTO> getAllFilmsWithDirectors() {
        return filmWithDirectorsMapper.toDTOs(repository.findAll());
    }

}
