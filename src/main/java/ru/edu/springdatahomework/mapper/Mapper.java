package ru.edu.springdatahomework.mapper;

import ru.edu.springdatahomework.dto.GenericDTO;
import ru.edu.springdatahomework.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtoList);

    List<D> toDTOs(List<E> entityList);
}
