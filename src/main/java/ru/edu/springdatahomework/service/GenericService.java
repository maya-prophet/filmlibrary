package ru.edu.springdatahomework.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.springdatahomework.dto.GenericDTO;
import ru.edu.springdatahomework.mapper.GenericMapper;
import ru.edu.springdatahomework.model.GenericModel;
import ru.edu.springdatahomework.repository.GenericRepository;

import java.util.List;


@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {

    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper){
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> getAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Элемент по этому ID не найден")));
    }

    public N create(N newObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updatedObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }

    public void delete(final Long id) {
        repository.deleteById(id);
    }
}
