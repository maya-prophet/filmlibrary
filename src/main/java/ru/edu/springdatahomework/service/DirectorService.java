package ru.edu.springdatahomework.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.springdatahomework.dto.DirectorDTO;
import ru.edu.springdatahomework.dto.DirectorWithFilmsDTO;
import ru.edu.springdatahomework.mapper.DirectorMapper;
import ru.edu.springdatahomework.mapper.DirectorWithFilmsMapper;
import ru.edu.springdatahomework.model.Director;
import ru.edu.springdatahomework.model.Film;
import ru.edu.springdatahomework.repository.DirectorRepository;
import ru.edu.springdatahomework.repository.FilmRepository;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director, DirectorDTO> {

    protected final FilmRepository filmRepository;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;

    public DirectorService(DirectorMapper mapper,
                           DirectorRepository directorRepository,
                           FilmRepository filmRepository,
                           DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(directorRepository, mapper);
        this.filmRepository = filmRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;
    }

    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Director director = repository.findById(directorId)
                .orElseThrow(() -> new NotFoundException("Директор с таким ID не найден"));
        Film film = filmRepository.findById(filmId)
                .orElseThrow(() -> new NotFoundException("Фильм с таким ID не найден"));
        director.getFilms().add(film);
        return mapper.toDTO(repository.save(director));
    }

    public List<DirectorWithFilmsDTO> getAllDirectorsWithFilms () {
        return directorWithFilmsMapper.toDTOs(repository.findAll());
    }

}
