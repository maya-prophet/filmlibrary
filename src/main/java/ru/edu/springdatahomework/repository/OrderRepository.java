package ru.edu.springdatahomework.repository;

import org.springframework.stereotype.Repository;
import ru.edu.springdatahomework.model.Order;

@Repository
public interface OrderRepository extends GenericRepository<Order>{
}
