package ru.edu.springdatahomework.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.springdatahomework.dto.OrderResponseDTO;
import ru.edu.springdatahomework.dto.OrderRequestDTO;
import ru.edu.springdatahomework.mapper.OrderMapper;
import ru.edu.springdatahomework.model.Order;
import ru.edu.springdatahomework.model.User;
import ru.edu.springdatahomework.model.Film;
import ru.edu.springdatahomework.repository.FilmRepository;
import ru.edu.springdatahomework.repository.OrderRepository;
import ru.edu.springdatahomework.repository.UserRepository;

import java.time.LocalDate;

@Service
public class OrderService extends GenericService<Order, OrderResponseDTO> {

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;

    protected OrderService(OrderRepository repository,
                           OrderMapper mapper,
                           UserRepository userRepository,
                           FilmRepository filmRepository) {
        super(repository, mapper);
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    public OrderResponseDTO orderFilm(OrderRequestDTO orderRequestDTO) {
        User user = userRepository.findById(orderRequestDTO.getUserId())
                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден"));
        Film film = filmRepository.findById(orderRequestDTO.getFilmId())
                .orElseThrow(() -> new NotFoundException("Фильм с таким ID не найден"));
        Order order = new Order();
        order.setFilm(film);
        order.setUser(user);
        if (orderRequestDTO.isPurchase())
            order.setPurchase(true);
        else {
            order.setPurchase(false);
            order.setRentDate(LocalDate.now());
            order.setRentPeriod(orderRequestDTO.getRentPeriod());
        }
        repository.save(order);
        return mapper.toDTO(order);
    }

}
