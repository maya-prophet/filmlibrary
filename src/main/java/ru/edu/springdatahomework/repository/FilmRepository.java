package ru.edu.springdatahomework.repository;

import org.springframework.stereotype.Repository;
import ru.edu.springdatahomework.model.Film;

@Repository
public interface FilmRepository extends GenericRepository<Film>{
}
