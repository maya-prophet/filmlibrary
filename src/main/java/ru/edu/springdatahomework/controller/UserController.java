package ru.edu.springdatahomework.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.edu.springdatahomework.dto.FilmDTO;
import ru.edu.springdatahomework.dto.UserDTO;
import ru.edu.springdatahomework.model.User;
import ru.edu.springdatahomework.service.UserService;

import java.util.Set;


@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями фильмотеки")
public class UserController extends GenericController<User, UserDTO> {

    private final UserService service;

    public UserController(UserService service) {
        super(service);
        this.service = service;
    }

    @Operation(description = "Получить список всех арендованных / купленных фильмов", method = "getFilmList")
    @GetMapping(value = "/filmList", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<FilmDTO>> getFilmList(@RequestParam(value = "id") Long userId) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.getFilmList(userId));
    }
}
