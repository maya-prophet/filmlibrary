package ru.edu.springdatahomework.model;

public enum Genre {
    ACTION("Экшен"),
    COMEDY("Комедия"),
    DRAMA("Драма"),
    FANTASY("Фэнтэзи"),
    SCIENCE_FICTION("Научная фантастика"),
    HORROR("Фильм ужасов"),
    MYSTERY("Детективный фильм"),
    ROMANCE("Романтический фильм"),
    THRILLER("Триллер"),
    WESTERN("Вестерн"),
    ANIMATION("Анимационный фильм"),
    OTHER("Другой");

    private final String genreName;

    Genre(String genreName) {
        this.genreName = genreName;
    }

    public String getGenreName() {
        return genreName;
    }
}
