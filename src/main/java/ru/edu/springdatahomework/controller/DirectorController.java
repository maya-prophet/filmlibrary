package ru.edu.springdatahomework.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.edu.springdatahomework.dto.DirectorDTO;
import ru.edu.springdatahomework.model.Director;
import ru.edu.springdatahomework.service.DirectorService;

@RestController
@RequestMapping("/directors")
@Tag(name = "Директоры",
        description = "Контроллер для работы с директорами фильмов фильмотеки")

public class DirectorController extends GenericController<Director, DirectorDTO> {

    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }

    @Operation(description = "Добавить фильм к директору", method = "addFilm")
    @PostMapping(value = "/addFilm", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "directorId") Long directorId,
                                               @RequestParam(value = "filmId") Long filmId) {
        return ResponseEntity.status(HttpStatus.OK).body(directorService.addFilm(directorId, filmId));
    }
}
