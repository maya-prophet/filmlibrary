package ru.edu.springdatahomework.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponseDTO extends GenericDTO {
//    private UserDTO user;
//    private FilmDTO film;

    private LocalDate rentDate;

    private Integer rentPeriod;

    private Boolean purchase = false;

    private Long filmId;

    private Long userId;
}
