package ru.edu.springdatahomework.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.edu.springdatahomework.dto.OrderResponseDTO;
import ru.edu.springdatahomework.dto.OrderRequestDTO;
import ru.edu.springdatahomework.model.Order;
import ru.edu.springdatahomework.service.OrderService;

@RestController
@RequestMapping("/order")
@Tag(name = "Аренда фильмов",
    description = "Контроллер для работы с арендой фильмов")
public class OrderController extends GenericController<Order, OrderResponseDTO> {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService =  orderService;
    }

    @Operation(description = "Создать новый заказ", method = "putOrder")
    @PostMapping(value = "/putOrder", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderResponseDTO> putOrder(@RequestBody OrderRequestDTO orderRequestDTO) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.orderFilm(orderRequestDTO));
    }

}
