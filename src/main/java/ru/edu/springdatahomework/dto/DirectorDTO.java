package ru.edu.springdatahomework.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DirectorDTO extends GenericDTO {
    private String directorsFio;

    private String position;

    private Set<Long> filmsIds;
}
