package ru.edu.springdatahomework.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.edu.springdatahomework.model.Country;
import ru.edu.springdatahomework.model.Genre;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO extends GenericDTO {
    private String title;

    private Integer premierYear;

    private Country country;

    private Genre genre;

    private Set<Long> directorsIds;

}
