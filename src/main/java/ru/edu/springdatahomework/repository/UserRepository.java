package ru.edu.springdatahomework.repository;

import org.springframework.stereotype.Repository;
import ru.edu.springdatahomework.model.User;


@Repository
public interface UserRepository extends GenericRepository<User>{

    User findUserByLogin(String login);

    User findUserByEmail(String email);

}
