package ru.edu.springdatahomework.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.edu.springdatahomework.dto.FilmDTO;
import ru.edu.springdatahomework.model.Film;
import ru.edu.springdatahomework.service.FilmService;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы",
    description = "Контроллер для работы с фильмами фильмотеки")
public class FilmController extends GenericController<Film, FilmDTO> {
    private final FilmService filmService;

    public FilmController(FilmService filmService) {
        super(filmService);
        this.filmService = filmService;
    }

    @Operation(description = "Добавить директора к фильму", method = "addDirector")
    @PostMapping(value = "/addDirector", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
                                            @RequestParam(value = "directorId") Long directorId) {

        return ResponseEntity.status(HttpStatus.OK).body(filmService.addDirector(filmId, directorId));
    }
}
