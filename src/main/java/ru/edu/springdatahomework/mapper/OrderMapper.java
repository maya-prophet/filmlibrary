package ru.edu.springdatahomework.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.edu.springdatahomework.dto.OrderResponseDTO;
import ru.edu.springdatahomework.model.Order;
import ru.edu.springdatahomework.repository.FilmRepository;
import ru.edu.springdatahomework.repository.UserRepository;

import java.util.Objects;
import java.util.Set;

@Component
public class OrderMapper extends GenericMapper<Order, OrderResponseDTO> {
//    private final UserRepository userRepository;
//    private final FilmRepository filmRepository;

//    public OrderMapper(ModelMapper modelMapper, UserRepository userRepository, FilmRepository filmRepository) {
//        super(modelMapper, Order.class, OrderResponseDTO.class);
//        this.userRepository = userRepository;
//        this.filmRepository = filmRepository;
//    }

    public OrderMapper(ModelMapper modelMapper) {
        super(modelMapper, Order.class, OrderResponseDTO.class);
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderResponseDTO.class)
                .addMappings(m -> m.skip(OrderResponseDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderResponseDTO::setFilmId)).setPostConverter(toDTOConverter());
//        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
//                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
//                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter());
    }

//    @Override
//    protected void mapSpecificFields(OrderDTO source, Order destination) {
//        destination.setUser(userRepository.findById(source.getUserId())
//                            .orElseThrow(() -> new NotFoundException("Пользователь не найдено")));
//        destination.setFilm(filmRepository.findById(source.getFilmId())
//                            .orElseThrow(() -> new NotFoundException("Фильм не найден")));
//    }

    @Override
    protected void mapSpecificFields(OrderResponseDTO source, Order destination) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

    @Override
    protected void mapSpecificFields(Order source, OrderResponseDTO destination) {
        if (Objects.nonNull(source)) {
            destination.setUserId(source.getUser().getId());
            destination.setFilmId(source.getFilm().getId());
        }
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
