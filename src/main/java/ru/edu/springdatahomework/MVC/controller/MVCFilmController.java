package ru.edu.springdatahomework.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.edu.springdatahomework.dto.DirectorDTO;
import ru.edu.springdatahomework.dto.FilmDTO;
import ru.edu.springdatahomework.dto.FilmWithDirectorsDTO;
import ru.edu.springdatahomework.service.DirectorService;
import ru.edu.springdatahomework.service.FilmService;

import java.util.List;

@Hidden
@Controller
@RequestMapping("/films")
public class MVCFilmController {
    private final FilmService filmService;
    private final DirectorService directorService;

    public MVCFilmController(FilmService filmService,
                             DirectorService directorService) {
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<FilmWithDirectorsDTO> films = filmService.getAllFilmsWithDirectors();
        List<DirectorDTO> directors = directorService.getAll();
        model.addAttribute("films", films);
        model.addAttribute("directors", directors);
        return "films/viewAllFilms";
    }

    @GetMapping("/add")
    public String create() {
        return "films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO) {
        filmService.create(filmDTO);
        return "redirect:/films";
    }


    @PostMapping("{filmId}/addDirector")
    public String addDirector(@ModelAttribute("addDirectorForm") DirectorDTO director,
                              @PathVariable("filmId") Long filmId) {

        filmService.addDirector(filmId, director.getId());
        return "redirect:/films";
    }
}
