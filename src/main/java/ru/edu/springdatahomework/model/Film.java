package ru.edu.springdatahomework.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "films")
@SequenceGenerator(name = "default_gen", sequenceName = "films_seq", allocationSize = 1)
@NoArgsConstructor
@Getter
@Setter
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Film extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "premier_year", nullable = false)
    private int premierYear;

    @Column(name = "country", nullable = false)
    @Enumerated
    private Country country;

    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre genre;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "film_directors",
                joinColumns = @JoinColumn(name = "film_id"),
                foreignKey = @ForeignKey(name = "FK_FILM_DIRECTORS"),
                inverseJoinColumns = @JoinColumn(name = "director_id"),
                inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILM"))
    private Set<Director> directors;

    //Нужно ли удалять заказы, если мы удалили фильм?
    @OneToMany(mappedBy = "film", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Order> orders;


}
