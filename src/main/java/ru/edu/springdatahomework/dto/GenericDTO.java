package ru.edu.springdatahomework.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public abstract class GenericDTO implements Serializable {
    private Long id;

}
