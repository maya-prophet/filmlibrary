package ru.edu.springdatahomework.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.edu.springdatahomework.dto.UserDTO;
import ru.edu.springdatahomework.model.GenericModel;
import ru.edu.springdatahomework.model.User;
import ru.edu.springdatahomework.repository.OrderRepository;
import ru.edu.springdatahomework.utils.DateFormatter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {

    private final OrderRepository orderRepository;

    public UserMapper(ModelMapper modelMapper, OrderRepository orderRepository) {
        super(modelMapper, User.class, UserDTO.class);
        this.orderRepository = orderRepository;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrdersIds))
                .setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders))
                .setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setBirthDate))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if(Objects.nonNull(source.getOrdersIds())) {
            destination.setOrders(new HashSet<>(orderRepository.findAllById(source.getOrdersIds())));
        } else {
            destination.setOrders(null);
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrdersIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(User user) {
        return Objects.isNull(user) || Objects.isNull(user.getOrders())
                ? null
                : user.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
