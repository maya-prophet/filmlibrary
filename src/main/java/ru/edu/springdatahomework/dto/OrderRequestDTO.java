package ru.edu.springdatahomework.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequestDTO extends GenericDTO{

    private int rentPeriod;

    private boolean purchase = false;

    private Long filmId;

    private Long userId;
}
