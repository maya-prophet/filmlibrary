package ru.edu.springdatahomework.repository;

import org.springframework.stereotype.Repository;
import ru.edu.springdatahomework.model.Director;

@Repository
public interface DirectorRepository extends GenericRepository<Director>{
}
