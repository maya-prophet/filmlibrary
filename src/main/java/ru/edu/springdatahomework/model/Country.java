package ru.edu.springdatahomework.model;

public enum Country {
    USA("США"),
    INDIA("Индия"),
    JAPAN("Япония"),
    FRANCE("Франция"),
    UNITED_KINGDOM("Великобритания"),
    CANADA("Канада"),
    GERMANY("Германия"),
    ITALY("Италия"),
    SPAIN("Испания"),
    PHILIPPINES("Филиппины"),
    CHINA("Китай"),
    MEXICO("Мексика"),
    SOUTH_KOREA("Южная Корея"),
    RUSSIA("Россия"),
    ARGENTINA("Аргентина"),
    AUSTRALIA("Австралия"),
    TURKEY("Турция"),
    BRAZIL("Бразилия"),
    BELGIUM("Бельгия"),
    NETHERLANDS("Нидерланды"),
    IRAN("Иран"),
    SWEDEN("Швеция"),
    INDONESIA("Индонезия"),
    POLAND("Польша"),
    SWITZERLAND("Швейцария"),
    AUSTRIA("Австрия"),
    DENMARK("Дания"),
    OTHER("Другая");

    private final String countryName;

    Country(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }
}
