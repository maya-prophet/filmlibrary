package ru.edu.springdatahomework.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.springdatahomework.dto.FilmDTO;
import ru.edu.springdatahomework.dto.RoleDTO;
import ru.edu.springdatahomework.dto.UserDTO;
import ru.edu.springdatahomework.mapper.FilmMapper;
import ru.edu.springdatahomework.mapper.UserMapper;
import ru.edu.springdatahomework.model.Order;
import ru.edu.springdatahomework.model.User;
import ru.edu.springdatahomework.repository.UserRepository;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserService extends GenericService<User, UserDTO>{

    private final UserRepository userRepository;
    private final FilmMapper filmMapper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    protected UserService(UserMapper mapper,
                          UserRepository userRepository,
                          FilmMapper filmMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userRepository, mapper);
        this.userRepository = userRepository;
        this.filmMapper = filmMapper;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        object.setCreatedWhen(LocalDate.now());
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

    public Set<FilmDTO> getFilmList(Long userId) {
        Set<FilmDTO> filmDTOS = new HashSet<>();
        User user = repository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Пользователь с таким ID не найден"));
        Set<Order> orders = user.getOrders();

        LocalDate today = LocalDate.now();
        for (Order order : orders) {
            if (order.isPurchase() ||
                    today.isAfter(order.getRentDate().minusDays(1)) &&
                            today.isBefore(order.getRentDate().plusDays(order.getRentPeriod() + 1))) {
                filmDTOS.add(filmMapper.toDTO(order.getFilm()));
            }
        }
        return filmDTOS;
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO((userRepository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO((userRepository).findUserByEmail(email));
    }

}
